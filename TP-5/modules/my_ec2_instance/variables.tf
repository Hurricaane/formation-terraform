variable "trigramme" {
  type = string
}

variable "instance_type" {
  type = string
  default = "t2.medium"
}
variable "tp" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "subnet_id" {
  type = string
}
variable "app" {
  type = string
}

variable "firewall_https" {
  type = bool
  default = false
}

variable "firewall_ssh" {
  type = bool
  default = true
}
