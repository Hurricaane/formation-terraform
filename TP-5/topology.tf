resource "aws_subnet" "my_subnet" {
  availability_zone = "${data.aws_region.current.name}c"
  cidr_block        = "10.0.5.0/24"
  vpc_id            = data.aws_vpc.my_vpc.id
  tags = {
    Name      = "subnet-TF-${var.tp}-${var.trigramme}"
    Formation = "terraform"
    User      = var.trigramme
  }
}

module "webserver" {
  source = "./modules/my_ec2_instance"

  app       = "webserver"
  trigramme = var.trigramme
  tp        = var.tp
  vpc_id    = data.aws_vpc.my_vpc.id
  subnet_id = aws_subnet.my_subnet.id
}

module "database" {
  source = "./modules/my_ec2_instance"

  app       = "database"
  trigramme = var.trigramme
  tp        = var.tp
  vpc_id    = data.aws_vpc.my_vpc.id
  subnet_id = aws_subnet.my_subnet.id
}