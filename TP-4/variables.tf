variable "trigramme" {
  type    = string
  default = "YAOU"
}

variable "tp" {
  type    = string
  default = "TP4"
}

variable "subnet_cidr_block" {
  type    = string
  default = "10.0.5.0/24"
}

variable "availability_zone_suffix" {
  type    = string
  default = "c"
}

variable "Formation" {
  type    = string
  default = "terraform"
}

variable "number_of_jenkins_slave" {
  type = number
}

variable "ingress_ports" {
  type = list(number)
}

variable "ingress_rules_configuration" {
  type = list(object({
    description = string
    port        = number
    protocol    = string
    cidr_blocks = list(string)
  }))
}